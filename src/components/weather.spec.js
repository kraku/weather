/*
 * Test file example
 */

import Vuex from 'vuex'
import ElementUI from 'element-ui'
import sinon from 'sinon'
import { createLocalVue, mount, shallowMount } from '@vue/test-utils'
import weather from './weather'

const localVue = createLocalVue()
const clickSearchStub = sinon.stub()

localVue.use(Vuex)
localVue.use(ElementUI)

const component = () => {
  return shallowMount(weather, {
    mocks: {
      $store: {
        state: {
          days: [{
            dt_txt: '2018-10-02 00:00:00'
          }, {
            dt_txt: '2018-10-03 09:00:00'
          }]
        },
        getters: {
          daysLabels: ['2018-10-02'],
          getDayByDtTxt () {}
        },
        dispatch () {
          return new Promise((resolve, reject) => {
            resolve()
          })
        }
      }
    },
    localVue
  })
}

describe('weather', () => {
  describe('getTime', () => {
    it('time should be taken from day dt_txt', () => {
      const wrapper = component()

      expect(wrapper.vm.getTime()).toBe('00:00')
    })
  })

  // more tests ...
})
