export default Object.freeze({
  OWP_KEY: 'b92568d60bb6812531964161a6794abd',
  MAPS_KEY: 'AIzaSyBwngFA_jPEQ9GfcpmGXR5d-qv6-EKEgt8',
  OWP_URL: 'https://api.openweathermap.org/data/2.5/forecast',
  IP_LOC_URL: 'https://ipapi.co/',
  IP_URL: 'https://api.ipify.org/?format=json',
  TIME_LABELS: ['00:00', '03:00', '06:00', '09:00', '12:00', '15:00', '18:00', '21:00']
})
