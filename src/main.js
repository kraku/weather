import Vue from 'vue'
import ElementUI from 'element-ui'
import * as VueGoogleMaps from 'vue2-google-maps'

import 'element-ui/lib/theme-chalk/index.css'

import app from './app'
import store from './store'
import config from './config'

Vue.config.productionTip = false

Vue.use(ElementUI)

Vue.use(VueGoogleMaps, {
  load: {
    key: config.MAPS_KEY,
    libraries: 'places'
  }
})

new Vue({
  el: '#app',
  components: { app },
  template: '<app/>',
  store
})
