// for testing
if (navigator.userAgent.indexOf('PhantomJS') > -1) {
  localStorage.clear()
}

export const mutations = {
  setCoords (state, coords) {
    state.coords = {
      lat: Number((coords.lat).toFixed(4)),
      lon: Number((coords.lon).toFixed(4))
    }

    localStorage.setItem('state.coords', JSON.stringify(state.coords))
  },

  setCity (state, city) {
    state.city = {
      name: city.name,
      country: city.country
    }

    localStorage.setItem('state.city', JSON.stringify(state.city))
  },

  addDay (state, day) {
    state.days.push(day)

    localStorage.setItem('state.days', JSON.stringify(state.days))
  },

  updateLastUpdate (state) {
    state.lastUpdate = new Date().getTime()

    localStorage.setItem('state.lastUpdate', JSON.stringify(state.lastUpdate))
  },

  initialiseState (state) {
    const states = {
      city: localStorage.getItem('state.city'),
      coords: localStorage.getItem('state.coords'),
      days: localStorage.getItem('state.days'),
      lastUpdate: localStorage.getItem('state.lastUpdate')
    }

    for (var i in states) {
      if (states.hasOwnProperty(i) && states[i]) {
        state[i] = JSON.parse(states[i])
      }
    }
  },

  clearCity (state) {
    state.city = []
  },

  clearCoords (state) {
    state.coords = []
  },

  clearDays (state) {
    state.days = []
  },

  clearLastUpdate (state) {
    state.lastUpdate = []
  }
}
