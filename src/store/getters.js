export const getters = {
  days: state => {
    return state.days.sort((a, b) => a.dt - b.dt)
  },

  firstTimeOfDay: state => {
    return state.days[0].dt_txt.match(/(?=\s)(.*)(?=:)/)[0].replace(/\s/, '')
  },

  getDayByDtTxt: state => dtTxt => state.days.find((x) => x.dt_txt === dtTxt),

  daysLabels: state => {
    const res = []
    let value

    for (var i in state.days) {
      if (state.days.hasOwnProperty(i)) {
        value = state.days[i].dt_txt.match(/^.+?(?=\s)/)[0]

        if (res.length === 5) {
          continue
        }

        if (res.indexOf(value) === -1) {
          res.push(value)
        }
      }
    }

    return res
  },

  city: state => {
    return state.city
  },

  lastUpdate: state => {
    return state.lastUpdate
  },

  coords: state => {
    return state.coords
  }
}
