import axios from 'axios'
import config from './../config'

export default {
  SET_FORECAST ({ state, commit }) {
    return new Promise((resolve, reject) => {
      if ((this.getters.lastUpdate !== null && new Date().getTime() - this.getters.lastUpdate <= 3600000)) {
        resolve()
        return
      }

      commit('clearDays')

      axios.get(
        `${config.OWP_URL}?lat=${this.getters.coords.lat}&lon=${this.getters.coords.lon}&units=metric&APPID=${config.OWP_KEY}`
      ).then(function (res) {
        res.data.list.forEach((day, index) => {
          commit('addDay', day)
        })

        commit('setCity', res.data.city)
        commit('updateLastUpdate')
        resolve()
      })
    })
  },

  GET_LOCATION_FROM_IP ({ commit }) {
    return axios.get(config.IP_URL).then(function (res) {
      return axios.get(config.IP_LOC_URL + res.data.ip + '/json')
    }).then(function (res) {
      return res.data
    })
  },

  SET_COORDS ({ state, commit }, coords) {
    commit('setCoords', coords)
  },

  CLEAR_ALL ({ state, commit }) {
    commit('clearCity')
    commit('clearCoords')
    commit('clearDays')
    commit('clearLastUpdate')

    localStorage.clear()
  }
}
