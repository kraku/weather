# Weather FooBar

> Weather FooBar - Frontend Developer Task

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm start

# run unit tests
npm test
```

## Process

It was a little bit like my personal hackathon, I worked on it for approximately 11 hours with small breaks. I will be more than happy to present the whole process of creating this app during video conversation, it just seems more natural to me.

## Tradeoffs 

I had to define goals which I could realistically achieve with limited time (10h). 

1. Widget for checking out weather in **current** area, not weather webpage
2. Location detection based on browser and external API
3. Design based mostly on css framework
4. 1% unit test coverage, just some example to proof that I know how to write them
5. Basic responsiveness, width 320px it is ;)

## What should&could be better

1. Better support for mobile devices
2. 80% unit test coverage
3. Nicer design
4. LESS/SASS/SCSS for css
5. Better localization screen, with search input etc.
6. Slider for time could work better with initial day

I feel that I could done a few things better but that should be enough for you to assess my frontend skills.

---
Have a nice day!